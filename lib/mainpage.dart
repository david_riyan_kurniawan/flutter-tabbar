import 'package:flutter/material.dart';
import 'package:test/homepage.dart';
import 'package:test/secondpage.dart';

class MainPage extends StatelessWidget {
  List<Tab> myTab = [
    Tab(
      icon: Icon(Icons.person),
    ),
    Tab(
      icon: Icon(Icons.message),
    )
  ];

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: myTab.length,
      child: Scaffold(
        appBar: AppBar(
            title: Text('Tabbar example'),
            bottom: TabBar(
              tabs: myTab,
              labelColor: Colors.amber,
              unselectedLabelColor: Colors.white,
            )),
        body: TabBarView(children: [HomePage(), SecondPage()]),
      ),
    );
  }
}
